# OpenML dataset: Urban-Dictionary-Terms

https://www.openml.org/d/43731

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
I scraped all of the currently available Urban Dictionary pages (611) on 3/26/17
Content

word - the slang term added to urban dictionary
definition - the definition of said term
author - the user account who contributed the term
tags - a list of the hashtags used
up - upvotes
down - downvotes
date - the date the term was added to Urban Dictionary

Acknowledgements
I would like to thank my good friend Neil for giving the idea to scrape these terms.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43731) of an [OpenML dataset](https://www.openml.org/d/43731). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43731/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43731/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43731/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

